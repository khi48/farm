from typing import Optional
from fastapi import BackgroundTasks, FastAPI, Request, WebSocket
# from fastapi.responses import StreamingResponse, FileResponse
from sh import tail
import json
import asyncio
import subprocess
from test import test

from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "http://192.168.1.136:3000",
    "http://192.168.1.136:*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return {"kieranisaloser": "World",
    "watermelonisdelicious": "yum"}
# from fastapi.templating import Jinja2Templates

# app = FastAPI()
# templates = Jinja2Templates(directory="templates")

# with open('file.txt', 'r') as file:
#     measurements = file.read()
#     # print(measurements)

# @app.get("/")
# def read_root(request: Request):
#     return templates.TemplateResponse("index.htm", {"request": request})


# https://stackoverflow.com/questions/57541356/how-to-send-the-output-of-a-long-running-python-script-over-a-websocket
@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()

    with subprocess.Popen(['python3', '-u', "test.py"],
                        stdout=subprocess.PIPE,
                        bufsize=1,
                        universal_newlines=True) as p:
        while True:
            print("'readinglines")
            line = p.stdout.readline()
            line = line.rstrip()
            print(f"line = {line}")
            await websocket.send_text(line)
            print("hmm")
            if not line: break
        # for line in process.stdout:



# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Optional[str] = None):
#     return {"item_id": item_id, "q": q}

@app.get("/test/{number}")
def run_function(number: int) -> str:
    output = test(number)
    print("output")
    print(output)
    return {"output": output}

@app.get("/test2/{number}")
def run_function(number: int) -> str:
    output = test(number)
    print("output")
    print(output)
    return {"output": output}

# def fake_video_streamer():
#     FileResponse("file.txt")
#     with open("file.txt", mode="rb") as file_like:  
#         yield from file_like  
    # for i in range(10):
    #     yield b"some fake video bytes"


# @app.get("/file_output")
# async def file_output():
#     return FileResponse("file.txt")
    # return StreamingResponse(fake_video_streamer())


# def write_notification(email: str, message=""):
#     with open("log.txt", mode="w") as email_file:
#         content = f"notification for {email}: {message}"
#         email_file.write(content)



# background task
# @app.post("/test/{number}")
# async def send_notification(number: int, background_tasks: BackgroundTasks):
#     background_tasks.add_task(test, number)
#     return {"message": f"python script running in background incrementing from {number}"}