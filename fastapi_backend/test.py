from datetime import datetime
import time

def test(number: int) -> str:
    print("running")
    for i in range(number):
        print(f"test: {i}")
        # asyncio.sleep(2)
        time.sleep(2)
        # with open("file.txt", mode="a") as myfile:
        #     myfile.write("\n" + str(i))
    output = f"I just counted to {number}"
    print(output)
    return output



if __name__ == "__main__":
    test(120000)