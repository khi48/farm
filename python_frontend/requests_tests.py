import requests
ip = "192.168.1.69"
port = "8000"

# r = requests.get(f"http://{ip}:{port}/test/32")

# output = r.json()

# print(r)
# print(output)

import json
import websocket
import time
from threading import Thread

#ws = create_connection("ws://{ip}:{port}/ws")
# result =  ws.recv()

def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_reason):
    print("### closed ###")
    print(close_status_code)
    print(close_reason)

# def on_open(ws):
#     def run(*args):
#         print("Hello %d" % i)
#         print("thread terminating...")
#     Thread.start_new_thread(run, ())

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp(f"ws://{ip}:{port}/ws",
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close)

    # ws.on_open = on_open
    ws.run_forever()