import React, { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

  const fastApiAddr = "http://192.168.1.69:8000"
  const [outputString, setoutputString] = useState("Test String")
  const [count, setCount] = useState(0)
  // const [desc, setDesc] = useState('')

  // Read all todos
  useEffect(() => {
    axios.get(`${fastApiAddr}/`)
      .then(res => {
        // setTodoList(res.data)
        console.log(res.data)
      })
  });

  const getTestOutput = () => {
    setoutputString("Getting Output...")
    axios.get(`${fastApiAddr}/test/${count}`)
      .then(
        res => {
          setoutputString(res.data.output)
        })
  };

  return (
    <div>
      <div className="App">
        Hello world
      </div>

      <div>
        <input onChange={event => setCount(event.target.value)} placeholder='Count' />
        <button onClick={getTestOutput}>Get Output</button>
      </div>

      <div>
        {outputString}
      </div>
    </div>
  );
}

export default App;